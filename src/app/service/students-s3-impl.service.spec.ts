import { TestBed } from '@angular/core/testing';

import { StudentsS3ImplService } from './students-s3-impl.service';

describe('StudentsS3ImplService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StudentsS3ImplService = TestBed.get(StudentsS3ImplService);
    expect(service).toBeTruthy();
  });
});
